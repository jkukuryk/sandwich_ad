import { LevelManager } from "src/stage/Levels/levels";
import {
  Scene,
  PerspectiveCamera,
  WebGLRenderer,
  Color,
  DirectionalLight,
  Object3D,
  HemisphereLight,
  Group,
} from "three";
import { ListenersManager } from "./eventHandler";
import { v4 as uuid } from "uuid";
import { Position } from "src/stage/Levels/types/levelItems";
import { TextManager } from "src/stage/Text/text";
import { TutorialManager } from "src/stage/Tutorial/tutorial";

class Game {
  scene: Scene;
  camera: PerspectiveCamera;
  renderer: WebGLRenderer;
  cube: Object3D;
  direction: number;
  dragData: Position;

  constructor() {
    this.Start = this.Start.bind(this);
    this._animate = this._animate.bind(this);
    this._createLight = this._createLight.bind(this);
    this._setCamera = this._setCamera.bind(this);
    this.dragData = { x: 0, y: 0 };
  }

  public Start() {
    ListenersManager.createListeners();
    this.scene = new Scene();
    this.scene.background = new Color().setRGB(1, 1, 1);
    this._setRenderer();
    this._createLight();
    this._setCamera();
    this.renderer.render(this.scene, this.camera);
    TextManager.Create();
    LevelManager.Start();
    TutorialManager.Create();
    this._animate();
  }

  private _createLight() {
    const hemiLight = new HemisphereLight(0xffffff, 0x666666);
    hemiLight.position.set(100, 200, 200);
    this.scene.add(hemiLight);
    const lightSize = 30;
    const dirLight = new DirectionalLight(0xffffff, 0.1);
    dirLight.position.set(20, 100, -10);
    dirLight.lookAt(0, 0, 0);
    dirLight.castShadow = true;
    dirLight.shadow.camera.top = lightSize;
    dirLight.shadow.camera.bottom = -lightSize;
    dirLight.shadow.camera.left = -lightSize;
    dirLight.shadow.camera.right = lightSize;
    dirLight.shadow.mapSize.width = 1920;
    dirLight.shadow.mapSize.height = 1080;
    this.scene.add(dirLight);
  }

  private _setRenderer() {
    this.renderer = new WebGLRenderer({ antialias: true });
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.shadowMap.enabled = true;
    document.getElementById("game_container").appendChild(this.renderer.domElement);
  }
  public resetCamera() {
    this.camera.position.set(0, 50, 12);
    this.camera.lookAt(0, 0, 0);
  }
  private _setCamera() {
    this.camera = new PerspectiveCamera(60, window.innerWidth / window.innerHeight, 0.1, 1000);
    this.resetCamera();
  }

  private _animate() {
    requestAnimationFrame(this._animate);
    this.renderer.render(this.scene, this.camera);
  }

  public onResize() {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
  }

  public addToScene(mesh: Object3D) {
    mesh.uuid = uuid();
    this.scene.add(mesh);
    return mesh.uuid;
  }

  public removeFromScene(mesh: Group | Object3D) {
    mesh.clear();
    this.scene.remove(mesh);
  }
}

export const GameInstance = new Game();
