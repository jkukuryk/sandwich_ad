import { Asset, AssetType } from "src/assets";
import { AssetsNames } from "src/constants/assetsNames";
import { Texture, TextureLoader } from "three";

import { GLTF, GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader.js";

type LoadedAsset = { source: GLTF | Texture } & Asset;

class LoaderClass {
  assets: LoadedAsset[];

  constructor() {
    this.assets = [] as LoadedAsset[];
  }

  public getAsset(name: AssetsNames) {
    const asset = this.assets.find((asset) => asset.name === name);
    return asset?.source;
  }

  public Load(assets: Asset[], callback?: (assets: Asset[]) => void) {
    let assetsToLoad = assets.length;

    const onError = (error: ErrorEvent) => {
      console.error(`cant load asset: ${error.toString()}`);
    };

    const onSuccess = (model: GLTF | Texture, asset: Asset) => {
      this._onAssetLoad(model, asset);
      assetsToLoad--;
      if (assetsToLoad === 0 && callback) {
        callback(this.assets);
      }
    };
    const loaderGLB = new GLTFLoader();
    const loaderImage = new TextureLoader();

    assets.forEach((asset) => {
      switch (asset.type) {
        case AssetType.GLB:
          loaderGLB.load(asset.src, (source) => onSuccess(source, asset), undefined, onError);
          break;
        case AssetType.PNG:
          loaderImage.load(asset.src, (source) => onSuccess(source, asset), undefined, onError);
          break;
        default:
          console.error("No handler for " + asset.type);
          break;
      }
    });
  }

  private _onAssetLoad(source: GLTF | Texture, assetObject: Asset) {
    this.assets.push({ ...assetObject, source });
  }
}

export const LoaderManager = new LoaderClass();
