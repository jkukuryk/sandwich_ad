import { GameInstance } from "./gameEngine";

class ListenersClass {
  constructor() {
    this.createListeners = this.createListeners.bind(this);
    this.removeListeners = this.removeListeners.bind(this);
  }

  public createListeners() {
    window.addEventListener("resize", this._onWindowResize);
  }

  public removeListeners() {
    window.removeEventListener("resize", this._onWindowResize);
  }

  private _onWindowResize() {
    GameInstance.onResize();
  }
}

export const ListenersManager = new ListenersClass();
