import { Object3D } from "three";

export function setOpacity(mesh: Object3D, value: number): void {
  //@ts-expect-error - material have opacity
  mesh.material.opacity = value;
}

export function addOpacity(mesh: Object3D, value: number): void {
  if (getOpacity(mesh) > 1 && value > 0) {
    //@ts-expect-error - material have opacity
    mesh.material.opacity = 1;
  } else if (getOpacity(mesh) < 0 && value < 0) {
    //@ts-expect-error - material have opacity
    mesh.material.opacity = 0;
  } else {
    //@ts-expect-error - material have opacity
    mesh.material.opacity += value;
  }
}
export function getOpacity(mesh: Object3D): number {
  //@ts-expect-error - material have opacity
  return mesh.material.opacity;
}
