import "reset-css";

import { GameInstance } from "classes/gameEngine";
import { LoaderManager } from "classes/loader";
import { assetsList } from "./assets";

LoaderManager.Load(assetsList, GameInstance.Start);
