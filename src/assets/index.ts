import sandwichModel from "assets/glb/sandwich.glb";
import saladModel from "assets/glb/salad.glb";
import cheeseModel from "assets/glb/cheese.glb";
import tomatoModel from "assets/glb/tomato.glb";
import plateModel from "assets/glb/plate.glb";

import sandwichTexture from "assets/images/sandwichTexture.png";
import tomatoTexture from "assets/images/tomatoTexture.png";
import saladTexture from "assets/images/saladTexture.png";
import cheeseTexture from "assets/images/cheeseTexture.png";
import plateTexture from "assets/images/plateTexture.png";
import nomTexture from "assets/images/nom.png";
import tapTexture from "assets/images/tapToEat.png";
import tryAgainTexture from "assets/images/tryAgain.png";
import awesomeTexture from "assets/images/awesome.png";
import ctaTexture from "assets/images/playNow.png";
import titleTexture from "assets/images/title.png";
import handTexture from "assets/images/hand.png";

import { AssetsNames } from "src/constants/assetsNames";

export enum AssetType {
  PNG = "png",
  GLB = "glb",
}

export type Asset = {
  name: AssetsNames;
  src: string;
  type: AssetType;
};

export const assetsList = [
  {
    type: AssetType.GLB,
    src: sandwichModel,
    name: AssetsNames.BREAD,
  },
  {
    type: AssetType.GLB,
    src: saladModel,
    name: AssetsNames.SALAD,
  },
  {
    type: AssetType.GLB,
    src: cheeseModel,
    name: AssetsNames.CHEESE,
  },
  {
    type: AssetType.GLB,
    src: tomatoModel,
    name: AssetsNames.TOMATO,
  },
  {
    type: AssetType.PNG,
    src: sandwichTexture,
    name: AssetsNames.BREAD_TEXTURE,
  },
  {
    type: AssetType.PNG,
    src: tomatoTexture,
    name: AssetsNames.TOMATO_TEXTURE,
  },
  {
    type: AssetType.PNG,
    src: saladTexture,
    name: AssetsNames.SALAD_TEXTURE,
  },
  {
    type: AssetType.PNG,
    src: cheeseTexture,
    name: AssetsNames.CHEESE_TEXTURE,
  },
  {
    type: AssetType.GLB,
    src: plateModel,
    name: AssetsNames.PLATE,
  },
  {
    type: AssetType.PNG,
    src: plateTexture,
    name: AssetsNames.PLATE_TEXTURE,
  },
  {
    type: AssetType.PNG,
    src: nomTexture,
    name: AssetsNames.NOM_TXT,
  },
  {
    type: AssetType.PNG,
    src: tapTexture,
    name: AssetsNames.TAP_TXT,
  },
  {
    type: AssetType.PNG,
    src: tryAgainTexture,
    name: AssetsNames.TRY_AGAIN_TXT,
  },
  {
    type: AssetType.PNG,
    src: awesomeTexture,
    name: AssetsNames.LEVEL_COMPLETE,
  },
  {
    type: AssetType.PNG,
    src: ctaTexture,
    name: AssetsNames.CTA,
  },
  {
    type: AssetType.PNG,
    src: titleTexture,
    name: AssetsNames.TITLE,
  },
  {
    type: AssetType.PNG,
    src: handTexture,
    name: AssetsNames.HAND,
  },
];
