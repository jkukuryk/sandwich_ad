import { GameInstance } from "src/classes/gameEngine";
import { LoaderManager } from "src/classes/loader";
import { AssetsNames } from "src/constants/assetsNames";
import { addOpacity, setOpacity } from "src/helper/opacity";
import { Mesh, MeshBasicMaterial, Texture } from "three";
import { Object3D, PlaneGeometry } from "three";

class TutorialClass {
  hand: Object3D;
  animationFrame: number;
  visible: boolean;

  constructor() {
    this._animation = this._animation.bind(this);
    this.animationFrame = 0;
    this.visible = false;
  }

  _makeMesh(asset: AssetsNames) {
    const plane = new PlaneGeometry(10, 10, 1, 1);
    const texture = LoaderManager.getAsset(asset) as Texture;
    const materialTexture = new MeshBasicMaterial({ map: texture, transparent: true });
    return new Mesh(plane, materialTexture);
  }

  Remove() {
    GameInstance.removeFromScene(this.hand);
    this.visible = false;
  }

  Create() {
    this.visible = true;
    this.hand = this._makeMesh(AssetsNames.HAND);
    GameInstance.addToScene(this.hand);
    this.hand.position.y = 5;
    setOpacity(this.hand, 0);
    this._animation();
  }

  private _animation() {
    if (this.visible) {
      window.requestAnimationFrame(this._animation);
      this.animationFrame++;
      addOpacity(this.hand, 0.01);
      const progress = Math.sin((Math.PI * this.animationFrame) / 100);
      this.hand.position.x = -10 + progress * 4;
      this.hand.lookAt(GameInstance.camera.position);
      this.hand.rotation.z = progress / 10;
    }
  }
}

export const TutorialManager = new TutorialClass();
