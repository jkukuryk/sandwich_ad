import { GameInstance } from "src/classes/gameEngine";
import { CELL_SIZE } from "src/constants/dimensions";
import { CircleGeometry, Mesh, MeshBasicMaterial, Vector3 } from "three";
import { Position } from "../Levels/types/levelItems";
import { staticRandom } from "src/helper/staticRandom";
import { addOpacity } from "src/helper/opacity";
type Particle = { size: number; mesh: Mesh; vector: Position };
const PARTICLE_EMIT_COUNT = 100;
const MAX_SIZE = 1.5;
const MIN_SIZE = 0.6;

class Particles {
  particles: Particle[];
  animationFrame: number;

  constructor() {
    this.particles = [] as Particle[];
    this._animation = this._animation.bind(this);
  }

  public Add(center: Vector3, color: number): void {
    const triggerAnimation = this.particles.length === 0;
    this.animationFrame = 0;
    for (let i = 0; i < PARTICLE_EMIT_COUNT; i++) {
      const size = staticRandom(i + 4) * (MAX_SIZE - MIN_SIZE) + MIN_SIZE;
      const circle = new CircleGeometry(size, 24);
      const material = new MeshBasicMaterial({ color, transparent: true });
      const mesh = new Mesh(circle, material);

      mesh.position.x = center.x + staticRandom(i) * CELL_SIZE - CELL_SIZE / 2;
      mesh.position.z = center.z + staticRandom(i + 10) * CELL_SIZE - CELL_SIZE / 2;
      mesh.position.y = center.y;
      mesh.rotation.x = -Math.PI / 2;
      const particleSpeed = 0.4;
      this.particles.push({
        size,
        mesh,
        vector: { x: (staticRandom(i + 9) - 0.5) * particleSpeed, y: (staticRandom(i + 12) - 0.5) * particleSpeed },
      });
      GameInstance.addToScene(mesh);
    }
    if (triggerAnimation) {
      this._animation();
    }
  }

  _animation(): void {
    if (this.particles.length > 0) {
      window.requestAnimationFrame(this._animation);
    }
    this.particles.forEach((particle: Particle, key: number) => {
      particle.mesh.position.x += particle.vector.x;
      particle.mesh.position.z += particle.vector.y;
      addOpacity(particle.mesh, -0.05);
      const scale = particle.mesh.scale.x - 0.05;
      particle.mesh.scale.set(scale, scale, scale);
      if (scale <= 0) {
        GameInstance.removeFromScene(particle.mesh);
        this.particles.splice(key, 1);
      }
    });
  }
}

export const ParticleManager = new Particles();
