import { GameInstance } from "src/classes/gameEngine";
import { CELL_HEIGHT } from "src/constants/dimensions";
import { Mesh, MeshLambertMaterial, Object3D, PlaneGeometry } from "three";

class FloorClass {
  ground: Object3D;

  Start() {
    const groundGeo = new PlaneGeometry(10000, 10000);
    const groundMat = new MeshLambertMaterial({ color: 0xffffff });

    this.ground = new Mesh(groundGeo, groundMat);
    this.ground.position.z = 0;
    this.ground.position.x = 0;
    this.ground.position.y = (CELL_HEIGHT / 2) * -1;
    this.ground.rotation.x = -Math.PI / 2;
    this.ground.receiveShadow = true;
    GameInstance.addToScene(this.ground);
  }
}

export const FloorManager = new FloorClass();
