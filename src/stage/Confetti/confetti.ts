import { GameInstance } from "src/classes/gameEngine";
import { Mesh, MeshBasicMaterial, Vector3, PlaneGeometry, Color } from "three";
import { Position3 } from "../Levels/types/levelItems";
import { staticRandom } from "src/helper/staticRandom";

type Confetti = {
  size: number;
  mesh: Mesh;
  vector: Position3;
  rotation: Position3;
};
const PARTICLE_EMIT_COUNT = 80;
const MAX_SIZE = 1.5;
const MIN_SIZE = 1;

class ConfettiClass {
  particles: Confetti[];
  animationFrame: number;

  constructor() {
    this.particles = [] as Confetti[];
    this._animation = this._animation.bind(this);
  }

  public Add(center: Vector3): void {
    const triggerAnimation = this.particles.length === 0;
    this.animationFrame = 0;
    for (let i = 0; i < PARTICLE_EMIT_COUNT; i++) {
      const size = staticRandom(i + 9) * (MAX_SIZE - MIN_SIZE) + MIN_SIZE;
      const plane = new PlaneGeometry(size, size);
      const colorWheel = 360 * staticRandom(i);
      const material = new MeshBasicMaterial({ color: new Color().setHSL(colorWheel, 1, 0.5), transparent: true });
      const mesh = new Mesh(plane, material);

      mesh.position.x = 0;
      mesh.position.z = 5;
      mesh.position.y = center.y;

      const particleSpeed = staticRandom(i) * 1;
      this.particles.push({
        size,
        mesh,
        vector: {
          x: (staticRandom(i + 9) - 0.5) * particleSpeed,
          y: staticRandom(i + 6) * particleSpeed,
          z: staticRandom(i + 12) * particleSpeed * -2,
        },
        rotation: {
          x: Math.PI * 0.1 * staticRandom(i),
          y: Math.PI * 0.1 * staticRandom(i + 3),
          z: Math.PI * 0.1 * staticRandom(i + 4),
        },
      });
      GameInstance.addToScene(mesh);
    }
    if (triggerAnimation) {
      this._animation();
    }
  }

  _animation(): void {
    this.animationFrame++;
    if (this.particles.length > 0) {
      window.requestAnimationFrame(this._animation);
    }
    this.particles.forEach((particle: Confetti, key: number) => {
      particle.mesh.position.x += particle.vector.x;
      particle.mesh.position.y += particle.vector.y;
      particle.mesh.position.z += particle.vector.z;

      particle.mesh.rotation.x += particle.rotation.x;
      particle.mesh.rotation.z += particle.rotation.z;
      particle.mesh.rotation.y += particle.rotation.y;
      const scale = particle.mesh.scale.x - 0.01;
      particle.mesh.scale.set(scale, scale, scale);
      if (scale <= 0) {
        GameInstance.removeFromScene(particle.mesh);
        this.particles.splice(key, 1);
      } else {
        particle.vector.y -= 0.01;
      }
    });
  }
}

export const ConfettiManager = new ConfettiClass();
