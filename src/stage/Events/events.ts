import { GameInstance } from "src/classes/gameEngine";
import { Raycaster, Vector2 } from "three";
import { LevelManager, LevelModes } from "../Levels/levels";
import { Direction, Position } from "../Levels/types/levelItems";
import { TextManager } from "../Text/text";
import { TutorialManager } from "../Tutorial/tutorial";

const MIN_VALID_MOVE_RANGE = 12;

class EventsClass {
  dragActive: boolean;
  moveBlock: boolean;
  dragStartPosition: Position;

  constructor() {
    this.dragActive = false;
    this.moveBlock = false;
    this._startDrag = this._startDrag.bind(this);
    this._makeMove = this._makeMove.bind(this);
  }

  Start() {
    GameInstance.renderer.domElement.addEventListener("mousedown", this._startDrag, false);
    GameInstance.renderer.domElement.addEventListener("mousemove", this._makeMove, false);
    GameInstance.renderer.domElement.addEventListener("mouseup", this._stopDrag, false);
  }

  private _startDrag(e: MouseEvent) {
    if (!this.moveBlock) {
      const raycaster = new Raycaster();
      const mouse = new Vector2();
      mouse.x = (e.clientX / window.innerWidth) * 2 - 1;
      mouse.y = -(e.clientY / window.innerHeight) * 2 + 1;
      raycaster.setFromCamera(mouse, GameInstance.camera);

      if (LevelManager.levelMode === LevelModes.PLAY) {
        for (let row = 0; row < LevelManager.currentLevel.length; row++) {
          for (let cell = 0; cell < LevelManager.currentLevel[row].length; cell++) {
            const targetGroup = LevelManager.getGroupOnPosition({ y: row, x: cell });
            if (targetGroup?.group) {
              const intersects = raycaster.intersectObjects(targetGroup.group.children);
              if (intersects.length > 0) {
                LevelManager.setActiveGroup(targetGroup);
                this.dragActive = true;
                this.dragStartPosition = { x: e.clientX, y: e.clientY };
                break;
              }
            }
          }
        }
      }
      if ([LevelModes.BITE_1, LevelModes.BITE_2, LevelModes.BITE_3].includes(LevelManager.levelMode)) {
        LevelManager.eat();
      } else if (LevelManager.levelMode === LevelModes.CTA) {
        const intersects = raycaster.intersectObjects([TextManager.cta]);
        if (intersects.length > 0) {
          window.location.href = "https://popcore.com/";
        }
      }
    }
  }

  public setBlockMove(state: boolean) {
    this.dragActive = false;
    this.moveBlock = state;
  }

  private _stopDrag() {
    this.dragActive = false;
  }

  private _makeMove(e: MouseEvent) {
    if (this.dragActive && LevelManager.activeGroup) {
      const diffX = e.offsetX - this.dragStartPosition.x;
      const diffY = e.offsetY - this.dragStartPosition.y;
      let direction;
      if (Math.abs(diffX) > Math.abs(diffY)) {
        direction = diffX > 0 ? Direction.RIGHT : Direction.LEFT;
      } else {
        direction = diffY > 0 ? Direction.DOWN : Direction.UP;
      }
      const dragValid = Math.max(Math.abs(diffX), Math.abs(diffY)) > MIN_VALID_MOVE_RANGE;

      if (dragValid) {
        TutorialManager.Remove();
        this.moveBlock = true;
        LevelManager.makeMove(direction);
        this.dragActive = false;
      }
    }
  }
}

export const EventsManager = new EventsClass();
