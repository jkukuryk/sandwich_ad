import { Group, Vector3 } from "three";
import { MeshTypes } from "../common/meshMap";

export type Position = { x: number; y: number };
export type Position3 = { x: number; y: number; z: number };

export enum Direction {
  UP = "up",
  DOWN = "down",
  LEFT = "left",
  RIGHT = "right",
}

export type AnimationGroup = {
  direction: Direction;
  targetCell: Position;
};

export type LevelCell = { group: Group; position: Vector3; gridPosition: Position; stashNames: MeshTypes[] } | null;
