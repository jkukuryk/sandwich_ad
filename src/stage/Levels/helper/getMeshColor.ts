import { MeshTypes } from "../common/meshMap";

const meshColors = {
  [MeshTypes.EMPTY]: 0x888888,
  [MeshTypes.BREAD]: 0xc47d4c,
  [MeshTypes.CHEESE]: 0xfdd72f,
  [MeshTypes.TOMATO]: 0xd33126,
  [MeshTypes.SALAD]: 0x6bb84d,
};

export function getColorByType(type: MeshTypes): number {
  if (meshColors[type]) {
    return meshColors[type];
  }
  return 0x000000;
}
