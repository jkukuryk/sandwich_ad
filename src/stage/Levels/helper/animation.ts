import { CELL_HEIGHT, CELL_SIZE } from "src/constants/dimensions";
import { Camera, Clock, MathUtils, Object3D, Vector3 } from "three";
import { Direction, LevelCell } from "../types/levelItems";

let animationProgress = 0;

const CLOCK = new Clock();

export function resetAnimation(): void {
  animationProgress = 0;
}
function getDeltaTime() {
  return 1 + CLOCK.getDelta();
}
export function animationFlip(activeGroup: LevelCell, targetGroup: LevelCell, direction: Direction): boolean {
  const ANIMATION_FRAMES = 22;
  const deltaTime = getDeltaTime();

  const targetGroupHeight = targetGroup.group.children.length * CELL_HEIGHT;
  const targetYStep = targetGroupHeight / ANIMATION_FRAMES;
  const currentGroupHeight = activeGroup.group.children.length * CELL_HEIGHT;
  animationProgress += deltaTime;
  activeGroup.group.position.y += targetYStep * deltaTime;
  activeGroup.group.translateY(currentGroupHeight / 2);

  switch (direction) {
    case Direction.DOWN:
      activeGroup.group.translateZ(CELL_SIZE / 2);
      activeGroup.group.rotation.x += (Math.PI / ANIMATION_FRAMES) * deltaTime;
      activeGroup.group.translateZ(-CELL_SIZE / 2);
      break;
    case Direction.UP:
      activeGroup.group.translateZ(-CELL_SIZE / 2);
      activeGroup.group.rotation.x -= (Math.PI / ANIMATION_FRAMES) * deltaTime;
      activeGroup.group.translateZ(CELL_SIZE / 2);
      break;
    case Direction.LEFT:
      activeGroup.group.translateX(-CELL_SIZE / 2);
      activeGroup.group.rotation.z += (Math.PI / ANIMATION_FRAMES) * deltaTime;
      activeGroup.group.translateX(CELL_SIZE / 2);
      break;
    case Direction.RIGHT:
      activeGroup.group.translateX(CELL_SIZE / 2);
      activeGroup.group.rotation.z -= (Math.PI / ANIMATION_FRAMES) * deltaTime;
      activeGroup.group.translateX(-CELL_SIZE / 2);
      break;
  }
  activeGroup.group.translateY(-currentGroupHeight / 2);

  const complete = animationProgress >= ANIMATION_FRAMES;

  if (complete) {
    resetAnimation();
  }

  return complete;
}

export function animationInvalidMove(activeGroup: LevelCell, y: number, direction: Direction): boolean {
  return animationShake(activeGroup, y, direction, 11);
}
export function animationFinishMove(activeGroup: LevelCell, y: number, direction: Direction): boolean {
  return animationShake(activeGroup, y, direction, 0);
}

function animationShake(activeGroup: LevelCell, y: number, direction: Direction, offsetFrame: number): boolean {
  const SHAKE_FRAMES = 22;
  const animPrc = (animationProgress + offsetFrame) / SHAKE_FRAMES;
  const maxAngel = 12;
  const sinusValue = (Math.sin(Math.PI * animPrc * 4) * (1 - animPrc)) / maxAngel;
  const deltaTime = getDeltaTime();
  animationProgress += deltaTime;

  switch (direction) {
    case "down":
      activeGroup.group.rotation.x = Math.PI * sinusValue;
      break;
    case "up":
      activeGroup.group.rotation.x = Math.PI * sinusValue * -1;
      break;
    case "left":
      activeGroup.group.rotation.z = Math.PI * sinusValue;
      break;
    case "right":
      activeGroup.group.rotation.z = Math.PI * sinusValue * -1;
      break;
  }
  activeGroup.group.position.y = y + Math.abs(sinusValue) * 4;
  const complete = animationProgress + offsetFrame >= SHAKE_FRAMES;
  if (complete) {
    resetAnimation();
  }

  return complete;
}

export function animationFallFood(levelCells: LevelCell[][]): boolean {
  let complete = true;
  const deltaTime = getDeltaTime();

  levelCells.forEach((row) => {
    row.forEach((cell) => {
      if (cell && cell.group.position.y > CELL_HEIGHT / 2) {
        cell.group.position.y -= 2 * deltaTime;
        complete = false;
      } else if (cell && cell.group.position.y !== CELL_HEIGHT / 2) {
        cell.group.position.y = CELL_HEIGHT / 2;
      }
    });
  });
  return complete;
}

export function animationFinishLevelFade(activeGroup: LevelCell, plate: Object3D, camera: Camera): boolean {
  const ANIMATION_COMPLETE = 60;
  const TOTAL_ANIMATION = 120;

  const targetPlatePositionY = 9;
  const targetPlatePositionX = 0;
  const targetPlatePositionZ = 0;
  const animPrc =
    animationProgress < ANIMATION_COMPLETE
      ? animationProgress / ANIMATION_COMPLETE
      : animationProgress + ANIMATION_COMPLETE / TOTAL_ANIMATION;

  const deltaTime = getDeltaTime();
  animationProgress += deltaTime;
  plate.position.y = plate.position.y < targetPlatePositionY ? (plate.position.y += 0.9) : targetPlatePositionY;
  plate.position.x = plate.position.x < targetPlatePositionX ? (plate.position.x += 1) : targetPlatePositionX;
  plate.position.z = plate.position.z > targetPlatePositionZ ? (plate.position.z -= 1) : targetPlatePositionZ;
  const sandwichPosition = activeGroup.group.position;
  const diffX = sandwichPosition.x - targetPlatePositionX;
  const diffZ = sandwichPosition.z - targetPlatePositionZ;
  const flatMoveStep = 0.2;

  if (diffX > targetPlatePositionX + flatMoveStep) {
    activeGroup.group.position.x -= flatMoveStep;
  } else if (diffX < targetPlatePositionX - flatMoveStep) {
    activeGroup.group.position.x += flatMoveStep;
  } else {
    activeGroup.group.position.x = targetPlatePositionX;
  }
  if (diffZ > targetPlatePositionZ + flatMoveStep) {
    activeGroup.group.position.z -= flatMoveStep;
  } else if (diffZ < targetPlatePositionZ - flatMoveStep) {
    activeGroup.group.position.z += flatMoveStep;
  } else {
    activeGroup.group.position.z = targetPlatePositionZ;
  }
  if (activeGroup.group.position.y < targetPlatePositionY + CELL_HEIGHT / 2) {
    activeGroup.group.position.y += 0.6;
  } else {
    activeGroup.group.position.y = targetPlatePositionY + CELL_HEIGHT / 2;
  }
  const targetRotation = Math.PI * 2.64;

  if (activeGroup.group.rotation.y < targetRotation) {
    activeGroup.group.rotation.y += targetRotation * 0.006;
  } else {
    activeGroup.group.rotation.y = targetRotation;
  }
  if (camera.position.y > 40) {
    camera.position.y -= 1;
  } else {
    camera.position.y = 40;
  }
  if (camera.position.z > 30) {
    camera.position.z -= 1;
  } else {
    camera.position.z = 30;
  }
  const scalePrc = Math.sin(Math.PI * 2 * animPrc);
  if (animationProgress <= ANIMATION_COMPLETE) {
    activeGroup.group.children.forEach((mesh, key) => {
      const rotatePrc = Math.sin(Math.PI * animPrc);

      mesh.position.y += 0.2 * key * scalePrc;
      mesh.rotation.z = rotatePrc * key * -0.01;
      mesh.rotation.x = rotatePrc * key * 0.1;
    });
  } else {
    activeGroup.group.scale.y = MathUtils.lerp(0.95, 1.1, Math.abs(scalePrc));
  }
  camera.lookAt(new Vector3(0, activeGroup.group.position.y + 3, 0));
  const levelFinishAnimationComplete = TOTAL_ANIMATION <= animationProgress;
  if (levelFinishAnimationComplete) {
    resetAnimation();
  }

  return levelFinishAnimationComplete;
}

export function animationBite(activeGroup: LevelCell): boolean {
  const SHAKE_FRAMES = 22;
  const animPrc = animationProgress / SHAKE_FRAMES;
  const sinusValue = 1 + Math.sin(Math.PI * animPrc) * -0.05;
  const deltaTime = getDeltaTime();
  animationProgress += deltaTime;
  activeGroup.group.scale.y = sinusValue;

  const complete = animationProgress >= SHAKE_FRAMES;
  if (complete) {
    resetAnimation();
  }

  return complete;
}
