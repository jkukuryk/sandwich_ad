import { Group } from "three";
import { MeshTypes } from "../common/meshMap";
import { Direction, LevelCell, Position } from "../types/levelItems";

export function getTargetCell(position: Position, direction: Direction): Position {
  let newX = position.x;
  let newY = position.y;

  switch (direction) {
    case Direction.DOWN:
      newY += 1;
      break;
    case Direction.UP:
      newY -= 1;
      break;
    case Direction.LEFT:
      newX -= 1;
      break;
    case Direction.RIGHT:
      newX += 1;
      break;
  }
  return { x: newX, y: newY };
}

//numbers as string used if there will be more Meshed then 10
const meshMap = {
  "-": MeshTypes.EMPTY,
  "1": MeshTypes.BREAD,
  "2": MeshTypes.CHEESE,
  "3": MeshTypes.TOMATO,
  "4": MeshTypes.SALAD,
};

export function translateLevelSymbols(levelArray: string[]): MeshTypes[][] {
  const level = [] as MeshTypes[][];
  levelArray.forEach((levelRow) => {
    const row = [] as MeshTypes[];
    const rowItem = levelRow.split("");
    rowItem.forEach((item: MeshTypes) => {
      if (Object.keys(meshMap).includes(item)) {
        // @ts-expect-error - type handled above
        row.push(meshMap[item]);
      } else {
        row.push(MeshTypes.EMPTY);
      }
    });
    level.push(row);
  });
  return level;
}

export function getLeftGroups(levelsGroups: LevelCell[][]): LevelCell[] {
  const groups = [] as LevelCell[];

  levelsGroups.forEach((levelRow) => {
    levelRow.forEach((levelCell: LevelCell) => {
      if (levelCell?.group) {
        groups.push(levelCell);
      }
    });
  });
  return groups;
}
