import { MeshTypes } from "../common/meshMap";
import { LevelManager } from "../levels";
import { LevelCell } from "../types/levelItems";

export function checkWinConditions(levelsGroups: LevelCell[][]): boolean {
  const leftCells = [] as MeshTypes[][];
  levelsGroups.forEach((row) => {
    row.forEach((cell) => {
      if (cell?.stashNames?.length > 0) {
        leftCells.push(cell.stashNames);
      }
    });
  });
  if (leftCells.length === 1) {
    const firstCell = leftCells[0][0];
    const lastCell = leftCells[0][leftCells[0].length - 1];
    if (firstCell === MeshTypes.BREAD && lastCell === MeshTypes.BREAD) {
      return true;
    }
  }
  return false;
}

export function checkPosiblesMoves(levelsGroups: LevelCell[][]): boolean {
  let movePossible = false;
  levelsGroups.forEach((row) => {
    row.forEach((levelCell) => {
      if (levelCell && levelCell.stashNames.length > 0) {
        const { x, y } = levelCell.gridPosition;
        if (
          !!LevelManager.getGroupOnPosition({ x: x - 1, y: y })?.stashNames?.length ||
          !!LevelManager.getGroupOnPosition({ x: x, y: y - 1 })?.stashNames?.length ||
          !!LevelManager.getGroupOnPosition({ x: x, y: y + 1 })?.stashNames?.length ||
          !!LevelManager.getGroupOnPosition({ x: x + 1, y: y })?.stashNames?.length
        ) {
          movePossible = true;
        }
      }
    });
  });
  return movePossible;
}
