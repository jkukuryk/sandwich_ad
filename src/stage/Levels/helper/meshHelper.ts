import { LoaderManager } from "src/classes/loader";
import { AssetsNames } from "src/constants/assetsNames";
import { Mesh, MeshToonMaterial, Object3D, RepeatWrapping, Texture } from "three";
import { GLTF } from "three/examples/jsm/loaders/GLTFLoader";

function getAllChild(model: GLTF) {
  return model?.scene?.children;
}
export function getMeshes(glbFileName: AssetsNames): Object3D[] {
  return getAllChild(LoaderManager.getAsset(glbFileName) as GLTF);
}

export function createMesh(glbFileName: AssetsNames, textureName: AssetsNames): { [key: string]: Object3D } | null {
  const meshes = getMeshes(glbFileName);
  const returnMesh: { [key: string]: Object3D } = {};

  if (meshes && textureName) {
    meshes.forEach((mesh) => {
      const modelMesh = mesh.clone();
      const texture = LoaderManager.getAsset(textureName) as Texture;
      texture.wrapS = RepeatWrapping;
      texture.wrapT = RepeatWrapping;
      texture.repeat.set(1, 1);
      texture.rotation = Math.PI;

      modelMesh.castShadow = true;
      modelMesh.receiveShadow = true;
      const material = new MeshToonMaterial({
        map: texture,
      });

      modelMesh.traverse((child) => {
        if ((<Mesh>child).isMesh) {
          (<Mesh>child).material = material;
        }
      });
      returnMesh[mesh.name] = modelMesh;
    });
    return returnMesh;
  }
  return null;
}
