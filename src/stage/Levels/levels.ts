import { get } from "lodash";
import { GameInstance } from "src/classes/gameEngine";
import { AssetsNames } from "src/constants/assetsNames";
import { CELL_HEIGHT, CELL_SIZE } from "src/constants/dimensions";
import { staticRandom } from "src/helper/staticRandom";
import { Group, Object3D, Vector3 } from "three";
import { ConfettiManager } from "../Confetti/confetti";
import { EventsManager } from "../Events/events";
import { FloorManager } from "../Floor/floor";
import { TextManager, TextType } from "../Text/text";
import { ParticleManager } from "../Particles/particles";
import { levelsList } from "./common/levelsGrid";
import { LevelModels, MeshTypes } from "./common/meshMap";
import {
  animationBite,
  animationFallFood,
  animationFinishLevelFade,
  animationFinishMove,
  animationFlip,
  animationInvalidMove,
  resetAnimation,
} from "./helper/animation";
import { getColorByType } from "./helper/getMeshColor";
import { getLeftGroups, getTargetCell, translateLevelSymbols } from "./helper/gridHelper";
import { createMesh } from "./helper/meshHelper";
import { checkPosiblesMoves, checkWinConditions } from "./helper/winConditions";
import { LevelCell, AnimationGroup, Direction, Position } from "./types/levelItems";

enum AnimationType {
  FOOD_FALL = "food_fall",
  FLIP = "flip",
  AFTER_MOVE_SHAKE = "shake",
  INVALID_SHAKE = "invalid",
  IDLE = "idle",
  WIN = "win",
  BITE = "bite",
  CTA = "cta",
}
const PLATE_START_POSITION = new Vector3(-40, -3, 40);
export enum LevelModes {
  PLAY = "move",
  BITE_1 = "bite_1",
  BITE_2 = "bite_2",
  BITE_3 = "bite_3",
  RELOAD = "reload",
  CTA = "cta",
}

class LevelClass {
  level: number;
  currentLevel: LevelCell[][];
  activeGroup: LevelCell;
  targetPosition: AnimationGroup | null;
  plate: Object3D;
  animationType: AnimationType;
  levelMode: LevelModes;

  constructor() {
    this.level = 0;
    this.currentLevel = [] as LevelCell[][];
    this._animation = this._animation.bind(this);
    this.eat = this.eat.bind(this);
  }

  Start() {
    this.levelMode = LevelModes.PLAY;
    this._beforeStart();
    this._startLevel();
    this._animation();
    EventsManager.Start();
    FloorManager.Start();
  }

  public setActiveGroup(group: LevelCell) {
    this.activeGroup = group;
  }

  public getGroupOnPosition(position: Position) {
    return get(this.currentLevel, [position.y, position.x]);
  }

  public makeMove(direction: Direction) {
    const currentGridPosition = LevelManager.activeGroup.gridPosition;
    const targetCell = getTargetCell(currentGridPosition, direction);
    const targetGroup = this.getGroupOnPosition(targetCell);
    if (targetGroup) {
      this.animationType = AnimationType.FLIP;
      this.targetPosition = { direction, targetCell };
    } else {
      this.animationType = AnimationType.INVALID_SHAKE;
      this.targetPosition = { direction, targetCell: currentGridPosition };
    }
  }

  _beforeStart() {
    const plateModels = createMesh(AssetsNames.PLATE, AssetsNames.PLATE_TEXTURE);
    const key = Object.keys(plateModels)[0];
    this.plate = plateModels[key];
    this.plate.position.set(PLATE_START_POSITION.x, PLATE_START_POSITION.y, PLATE_START_POSITION.z);
    GameInstance.addToScene(this.plate);
    this._resetLevel();
  }
  private _resetLevel() {
    if (this.activeGroup) {
      GameInstance.removeFromScene(this.activeGroup.group);
    }

    this.currentLevel.forEach((row) => {
      row.forEach((cellGroup) => {
        if (cellGroup) {
          GameInstance.removeFromScene(cellGroup.group);
        }
      });
    });
    this.currentLevel = [] as LevelCell[][];
    this.activeGroup = null;
    this.targetPosition = null;
    this.animationType = AnimationType.IDLE;
    GameInstance.resetCamera();
    resetAnimation();
    this.plate.position.set(PLATE_START_POSITION.x, PLATE_START_POSITION.y, PLATE_START_POSITION.z);
  }
  private _startLevel() {
    this.levelMode = LevelModes.PLAY;
    const currentLevel = translateLevelSymbols(levelsList[this.level]);
    const levelHeight = currentLevel.length;
    const levelWidth = currentLevel[0].length;
    const startU = ((levelWidth * CELL_SIZE) / 2) * -1;
    const startV = ((levelWidth * CELL_SIZE) / 2) * -1;
    EventsManager.setBlockMove(true);
    this.animationType = AnimationType.FOOD_FALL;

    for (let row = 0; row < levelHeight; row++) {
      const v = startV + row * CELL_SIZE;
      this.currentLevel.push([] as LevelCell[]);
      for (let cell = 0; cell < levelWidth; cell++) {
        this.currentLevel[row][cell] = null;

        const cellType = currentLevel[row][cell];
        const u = startU + cell * CELL_SIZE;
        const position = {
          x: u,
          y: v,
        };
        if (!LevelModels[cellType]?.mesh) {
          continue;
        }
        const group = this._makeInitialGroup(LevelModels[cellType], position);
        if (!group) {
          continue;
        }

        this.currentLevel[row][cell] = {
          group,
          position: new Vector3(group.position.x, CELL_HEIGHT / 2, group.position.z), //reset position Y to 0
          gridPosition: { x: cell, y: row },
          stashNames: [cellType],
        };
      }
    }
  }

  private _makeInitialGroup(groupDef: { texture: AssetsNames | null; mesh: AssetsNames | null }, position: Position) {
    const modelMesh = createMesh(groupDef.mesh, groupDef.texture);
    if (modelMesh) {
      const group = new Group(); //custom group
      group.add(modelMesh.ALL);
      GameInstance.addToScene(group);
      group.position.z = position.y;
      group.position.x = position.x;
      group.position.y = 20 + staticRandom(position.y * 2 + position.x + 20) * CELL_HEIGHT * 20;
      return group;
    }
  }

  private _animation() {
    window.requestAnimationFrame(this._animation);
    let animationComplete = false;
    let direction = null;
    let targetCell = null;
    let targetGroup = null;

    if (this.targetPosition) {
      direction = this.targetPosition.direction;
      targetCell = this.targetPosition.targetCell;
      targetGroup = this.getGroupOnPosition(targetCell);
    }

    switch (this.animationType) {
      case AnimationType.FOOD_FALL:
        animationComplete = animationFallFood(this.currentLevel);
        if (animationComplete) {
          EventsManager.setBlockMove(false);
          this.animationType = AnimationType.IDLE;
        }
        break;
      case AnimationType.FLIP:
        animationComplete = animationFlip(this.activeGroup, targetGroup, direction);

        if (animationComplete) {
          this._mergeGroups(this.activeGroup, targetGroup);
          this.currentLevel[this.activeGroup.gridPosition.y][this.activeGroup.gridPosition.x] = null;
          const canMakeNextMove = checkPosiblesMoves(this.currentLevel);
          if (canMakeNextMove) {
            this.targetPosition = { direction, targetCell };
            this.activeGroup = targetGroup;
            this.animationType = AnimationType.AFTER_MOVE_SHAKE;
          } else {
            const win = checkWinConditions(this.currentLevel);
            EventsManager.setBlockMove(true);
            if (win) {
              TextManager.Show(TextType.TAP);
              this.animationType = AnimationType.WIN;
              const lastGroup = getLeftGroups(this.currentLevel);
              if (lastGroup.length) {
                const sandwich = lastGroup[0];
                this.activeGroup = sandwich;
              }
            } else {
              this.reload();
              TextManager.Show(TextType.TRY_AGAIN);
            }
          }
        }
        break;
      case AnimationType.INVALID_SHAKE:
      case AnimationType.AFTER_MOVE_SHAKE:
        const { y } = targetGroup.position;
        if (this.animationType === AnimationType.INVALID_SHAKE) {
          animationComplete = animationInvalidMove(this.activeGroup, y, direction);
        } else {
          animationComplete = animationFinishMove(this.activeGroup, y, direction);
        }

        if (animationComplete) {
          this.activeGroup.group.rotation.x = 0;
          this.activeGroup.group.rotation.z = 0;
          this.activeGroup.group.position.y = y;
          EventsManager.setBlockMove(false);
          this.animationType = AnimationType.IDLE;
        }
        break;
      case AnimationType.WIN:
        const complete = (animationComplete = animationFinishLevelFade(
          this.activeGroup,
          this.plate,
          GameInstance.camera,
        ));
        if (complete) {
          this.levelMode = LevelModes.BITE_1;
          EventsManager.setBlockMove(false);
          this.animationType = AnimationType.IDLE;
        }
        break;
      case AnimationType.BITE:
        const biteComplete = animationBite(this.activeGroup);
        if (biteComplete) {
          this.animationType = AnimationType.IDLE;
        }
        break;
    }
  }

  private _mergeGroups(activeGroup: LevelCell, targetGroup: LevelCell) {
    const newChilds = [] as Object3D[];
    const lastTargetGroup = targetGroup.group.children[targetGroup.group.children.length - 1];
    const targetTypesArray = targetGroup.stashNames;
    const activeTypesArray = activeGroup.stashNames;
    const lastTargetType = targetTypesArray[targetTypesArray.length - 1];
    targetGroup.stashNames = [...targetTypesArray, ...activeTypesArray.reverse()];
    activeGroup.stashNames = [];

    activeGroup.group.children.forEach((mesh) => {
      mesh.rotation.x = 0;
      mesh.rotation.z = 0;
      newChilds.push(mesh.clone());
    });
    const newChildsForMerge = newChilds.reverse();
    for (let index = 0; index < newChildsForMerge.length; index++) {
      const mesh = newChildsForMerge[index];
      if (mesh) {
        targetGroup.group.add(mesh);
      }
    }

    targetGroup.group.children.forEach((mesh, key) => {
      mesh.position.y = key * CELL_HEIGHT;
    });
    GameInstance.removeFromScene(activeGroup.group);
    ParticleManager.Add(
      new Vector3(
        targetGroup.group.position.x,
        lastTargetGroup.position.y + CELL_HEIGHT / 2 + 0.2,
        targetGroup.group.position.z,
      ),
      getColorByType(lastTargetType),
    );
  }
  public reload() {
    this.levelMode = LevelModes.RELOAD;
    this._resetLevel();
    this._startLevel();
  }

  public eat() {
    TextManager.Show(TextType.NOM);

    const eatenGroup = new Group();
    const startY = this.plate.position.y;
    this.activeGroup.group.children.forEach((child, key) => {
      const type = this.activeGroup.stashNames[key];
      let mesh: { [key: string]: Object3D };
      switch (type) {
        case MeshTypes.BREAD:
          mesh = createMesh(AssetsNames.BREAD, AssetsNames.BREAD_TEXTURE);
          break;
        case MeshTypes.SALAD:
          mesh = createMesh(AssetsNames.SALAD, AssetsNames.SALAD_TEXTURE);
          break;
        case MeshTypes.CHEESE:
          mesh = createMesh(AssetsNames.CHEESE, AssetsNames.CHEESE_TEXTURE);
          break;
        case MeshTypes.TOMATO:
          mesh = createMesh(AssetsNames.TOMATO, AssetsNames.TOMATO_TEXTURE);
          break;
      }
      const part = this.levelMode === LevelModes.BITE_1 ? mesh.BITE_1 : mesh.BITE_2;

      part.position.set(0, startY + CELL_HEIGHT * key + CELL_HEIGHT / 2, 0);
      eatenGroup.add(part);
    });
    GameInstance.addToScene(eatenGroup);
    this.activeGroup.group.clear();
    this.activeGroup.group = eatenGroup;
    eatenGroup.rotation.y = Math.PI * 1.9;
    ParticleManager.Add(
      new Vector3(0, this.plate.position.y + (this.activeGroup.group.children.length * CELL_HEIGHT) / 2, 0),
      getColorByType(MeshTypes.BREAD),
    );

    if (this.levelMode === LevelModes.BITE_1) {
      this.animationType = AnimationType.BITE;
      this.levelMode = LevelModes.BITE_2;
    } else if (this.levelMode === LevelModes.BITE_2) {
      this.animationType = AnimationType.BITE;
      this.levelMode = LevelModes.BITE_3;
    } else {
      this.level++;
      ConfettiManager.Add(this.plate.position);
      if (this.level >= levelsList.length - 1) {
        this._resetLevel();
        this.reload();
        this.levelMode = LevelModes.CTA;
        TextManager.Show(TextType.CTA);
      } else {
        this.reload();
        TextManager.Show(TextType.LEVEL_COMPLETE);
      }
    }
  }
}

export const LevelManager = new LevelClass();
