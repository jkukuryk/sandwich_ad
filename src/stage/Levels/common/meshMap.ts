import { AssetsNames } from "constants/assetsNames";

export enum MeshTypes {
  EMPTY = "__empty_space__",
  BREAD = "bread",
  CHEESE = "cheese",
  TOMATO = "tomato",
  SALAD = "salad",
}

type LevelModelsType = {
  [key in MeshTypes]: {
    texture: AssetsNames;
    mesh: AssetsNames;
  };
};

export const LevelModels: LevelModelsType = {
  [MeshTypes.EMPTY]: {
    texture: null,
    mesh: null,
  },
  [MeshTypes.BREAD]: {
    texture: AssetsNames.BREAD_TEXTURE,
    mesh: AssetsNames.BREAD,
  },
  [MeshTypes.CHEESE]: {
    texture: AssetsNames.CHEESE_TEXTURE,
    mesh: AssetsNames.CHEESE,
  },
  [MeshTypes.TOMATO]: {
    texture: AssetsNames.TOMATO_TEXTURE,
    mesh: AssetsNames.TOMATO,
  },
  [MeshTypes.SALAD]: {
    texture: AssetsNames.SALAD_TEXTURE,
    mesh: AssetsNames.SALAD,
  },
};
