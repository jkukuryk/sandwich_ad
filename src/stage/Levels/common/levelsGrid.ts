const level_1 = [
  "-----", // don't break prettier
  "-----",
  "-213-",
  "--1--",
  "-----",
];
const level_2 = [
  "-----", // don't break prettier
  "-----",
  "-24--",
  "-113-",
  "-----",
];
const level_3 = [
  "-----", // don't break prettier
  "-23--",
  "-4---",
  "-311-",
  "-----",
];
const level_4 = [
  "-2-3-", // don't break prettier
  "43142",
  "3221-",
  "-3212",
  "4342-",
];
export const levelsList = [level_1, level_2, level_3, level_4];
