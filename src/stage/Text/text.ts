import { GameInstance } from "src/classes/gameEngine";
import { LoaderManager } from "src/classes/loader";
import { AssetsNames } from "src/constants/assetsNames";
import { setOpacity, addOpacity, getOpacity } from "src/helper/opacity";
import { Mesh, MeshBasicMaterial, Texture } from "three";
import { Object3D, PlaneGeometry } from "three";
export enum TextType {
  NOM = "nom",
  TAP = "tap",
  TRY_AGAIN = "try_again",
  CTA = "cta",
  LEVEL_COMPLETE = "level_complete",
}
const MAX_NOM_STEP = 3;

class TextClass {
  nomText: Object3D;
  tapText: Object3D;
  tryAgain: Object3D;
  completeText: Object3D;
  title: Object3D;
  cta: Object3D;
  overlay: Object3D;
  animationFrame;
  nomStep: number;
  textType: TextType | null;

  constructor() {
    this.animationFrame = 0;
    this.nomStep = MAX_NOM_STEP;
    this._animation = this._animation.bind(this);
  }

  _makeMesh(asset?: AssetsNames) {
    const plane = new PlaneGeometry(10, 10, 1, 1);
    if (asset) {
      const texture = LoaderManager.getAsset(asset) as Texture;
      const materialTexture = new MeshBasicMaterial({ map: texture, transparent: true });
      return new Mesh(plane, materialTexture);
    } else {
      const materialColor = new MeshBasicMaterial({ color: 0xffffff, transparent: true });
      return new Mesh(plane, materialColor);
    }
  }

  Create() {
    this.nomText = this._makeMesh(AssetsNames.NOM_TXT);
    GameInstance.addToScene(this.nomText);

    this.tapText = this._makeMesh(AssetsNames.TAP_TXT);
    GameInstance.addToScene(this.tapText);

    this.tryAgain = this._makeMesh(AssetsNames.TRY_AGAIN_TXT);
    GameInstance.addToScene(this.tryAgain);

    this.completeText = this._makeMesh(AssetsNames.LEVEL_COMPLETE);
    GameInstance.addToScene(this.completeText);

    this.cta = this._makeMesh(AssetsNames.CTA);
    GameInstance.addToScene(this.cta);
    this.title = this._makeMesh(AssetsNames.TITLE);
    GameInstance.addToScene(this.title);
    this.overlay = this._makeMesh();
    GameInstance.addToScene(this.overlay);

    this._hideAll();
    this._animation();
  }

  _hideAll() {
    this.textType = null;

    this.nomText.visible = false;

    this.tapText.visible = false;

    this.tryAgain.visible = false;

    this.completeText.visible = false;

    this.cta.visible = false;
    this.title.visible = false;
    this.overlay.visible = false;
  }

  Show(type: TextType) {
    this.animationFrame = 0;
    this._hideAll();
    this.textType = type;

    switch (type) {
      case TextType.NOM:
        this.nomStep++;
        if (this.nomStep >= MAX_NOM_STEP) {
          this.nomStep = 0;
        }
        setOpacity(this.nomText, 1);
        this.nomText.lookAt(GameInstance.camera.position);
        const stepPosition = this._getNomPosition();
        this.nomText.rotation.z = this._getNomRotation();
        this.nomText.position.y = stepPosition.y;
        this.nomText.position.x = stepPosition.x;
        this.nomText.visible = true;
        break;
      case TextType.TAP:
        this.tapText.position.y = 15;
        this.tapText.position.x = 0;
        this.tapText.position.z = 15;
        this.tapText.visible = true;
        break;
      case TextType.TRY_AGAIN:
        this.tryAgain.position.y = 15;
        this.tryAgain.position.x = 0;
        this.tryAgain.position.z = 15;
        this.tryAgain.visible = true;

        break;
      case TextType.LEVEL_COMPLETE:
        setOpacity(this.completeText, 1);
        this.completeText.visible = true;
        break;
      case TextType.CTA:
        setOpacity(this.cta, 0);
        setOpacity(this.title, 0);
        setOpacity(this.overlay, 0);
        this.title.scale.x = 1.8;
        this.title.scale.y = 1.8;
        this.overlay.scale.x = 100;
        this.overlay.scale.y = 100;

        this.cta.visible = true;
        this.title.visible = true;
        this.overlay.visible = true;

        break;
    }
  }
  _getNomPosition() {
    switch (this.nomStep) {
      case 0:
        return { x: -5, y: 20 };
        break;
      case 1:
        return { x: 7, y: 20 };
        break;
      case 2:
        return { x: 0, y: 20 };
        break;
    }
  }
  _getNomRotation() {
    switch (this.nomStep) {
      case 0:
        return 0.09;
        break;
      case 1:
        return -0.12;
        break;
      case 2:
        return 0.05;
        break;
    }
  }

  private _animation() {
    window.requestAnimationFrame(this._animation);
    this.animationFrame++;

    switch (this.textType) {
      case TextType.NOM:
        addOpacity(this.nomText, -0.05);
        this.nomText.position.y += 0.1;
        this.nomText.lookAt(GameInstance.camera.position);
        this.nomText.rotation.z = this._getNomRotation();
        break;
      case TextType.TAP:
        const scale = 1.6 + Math.sin((Math.PI * this.animationFrame) / 10) * 0.03;
        this.tapText.scale.x = scale;
        this.tapText.scale.y = scale;
        this.tapText.scale.z = scale;
        this.tapText.lookAt(GameInstance.camera.position);
        break;
      case TextType.TRY_AGAIN:
        if (this.animationFrame > 20) {
          addOpacity(this.tryAgain, -0.05);
        }

        const scaleTry = 1 + this.animationFrame * 0.03;
        this.tryAgain.scale.x = scaleTry;
        this.tryAgain.scale.y = scaleTry;
        this.tryAgain.position.y = 25;
        this.tryAgain.position.x = 0;
        this.tryAgain.position.z = 0;
        this.tryAgain.lookAt(GameInstance.camera.position);
        this.tryAgain.rotation.z = 0;
        if (getOpacity(this.tryAgain) <= 0) {
          this._hideAll();
          this.animationFrame = 0;
          setOpacity(this.tryAgain, 1);
        }
        break;
      case TextType.LEVEL_COMPLETE:
        if (this.animationFrame > 40) {
          addOpacity(this.completeText, -0.05);
        }
        const completeScale = 1.6 + Math.sin((Math.PI * this.animationFrame) / 10) * 0.03;
        const rotateScale = Math.sin((Math.PI * this.animationFrame) / 10) * 0.03;

        this.completeText.scale.x = completeScale;
        this.completeText.scale.y = completeScale;
        this.completeText.scale.z = completeScale;

        this.completeText.position.y = 25;
        this.completeText.position.x = 0;
        this.completeText.position.z = 0;
        this.completeText.lookAt(GameInstance.camera.position);
        this.completeText.rotation.z = rotateScale;
        if (getOpacity(this.completeText) <= 0) {
          this._hideAll();
          this.animationFrame = 0;
          setOpacity(this.completeText, 1);
        }
        break;
      case TextType.CTA:
        const scaleCta = 2.5 + Math.sin((Math.PI * this.animationFrame) / 10) * 0.03;
        this.cta.scale.x = scaleCta;
        this.cta.scale.y = scaleCta;
        this.cta.scale.z = scaleCta;
        this.cta.position.y = 5;
        this.cta.position.x = 0;
        this.cta.position.z = 0;
        this.cta.lookAt(GameInstance.camera.position);
        this.cta.rotation.z = 0;

        this.title.position.y = 30;
        this.title.position.x = 0;
        this.title.position.z = 0;

        this.title.lookAt(GameInstance.camera.position);
        this.overlay.lookAt(GameInstance.camera.position);
        this.title.rotation.z = 0;
        this.overlay.position.x = 0;
        this.overlay.position.y = 2;
        this.overlay.position.z = 11;

        if (getOpacity(this.cta) < 1) {
          addOpacity(this.cta, 0.01);
          addOpacity(this.title, 0.01);
          addOpacity(this.overlay, 0.006);
        }

        break;
    }
  }
}

export const TextManager = new TextClass();
